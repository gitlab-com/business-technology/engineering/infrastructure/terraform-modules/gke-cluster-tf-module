## Cluster Sizing and Best Practices

Kubernetes cluster sizing involves a combination of right sizing and architectural limits for CPU, memory, pods per node, subnetwork CIDR range sizing, and understanding how CPU and memory intensive your containers will be. We have configured opinionated defaults that you can override with variables.

This cluster Terraform module supports a variety of configurations depending on the size of your `gcp_machine_type` and the `gke_node_pods_count_max`.

To be as efficient as possible with these design constraints and avoid saturating the CPU of a node, **we recommend 2 CPU cores and 32 pods per node**. For more intensive container workloads, you can increase to 4 or 8 CPUs. For more advanced use cases (outside the scope of this Terraform module), you can consider additional node pools with different machine types and Kubernetes taints.

## Cost Calculations

For basic cost calculation purposes, we use the [cluster management fee ($0.10/hr)](https://cloud.google.com/kubernetes-engine/pricing#cluster_management_fee_and_free_tier) and the [cost of the each node](https://cloud.google.com/compute/vm-instance-pricing#e2_predefined) that is running in the `us-east1` region. This does not include any storage or bandwidth cost that are usually miniscule comparatively.

Each cluster runs approximately 15 pods for Kubernetes management services as a minimum baseline.

| Machine Type    | Hourly Price | Monthly Price | Preemptible Monthly Price |
|-----------------|--------------|---------------|---------------------------|
| `e2-standard-2` | `$0.067006`  | `$48.91`      | `$14.67`                  |
| `e2-standard-4` | `$0.134012`  | `$97.83`      | `$29.35`                  |
| `e2-standard-8` | `$0.268024`  |`$195.66`      | `$58.70`                  |


| Machine Type    | Cluster Type | 3-Day 10 Pods (Preemptible) | Monthly Min Cost (Pre) | Monthly 100 Pods (Pre) |
|-----------------|--------------|-----------|-------------|-------------|
| `e2-standard-2` | `zonal`      | `$12 ($9)`  | `$122 ($88)`  | `$269 ($132)` |
| `e2-standard-2` | `regional`   | `$22 ($12)` | `$220 ($117)` | `$269 ($132)` |
| `e2-standard-4` | `zonal`      | `$17 ($10)` | `$171 ($102)` | `$464 ($190)` |
| `e2-standard-4` | `regional`   | `$36 ($16)` | `$366 ($161)` | `$464 ($190)` |
| `e2-standard-8` | `zonal`      | `$27 ($13)` | `$269 ($132)` | `$856 ($308)` |
| `e2-standard-8` | `regional`   | `$65 ($25)` | `$660 ($249)` | `$856 ($308)` |

This table assumes 32 pods per node for all machine types to show CPU and memory sizing implications.

```
( ( {Hourly Price} * {Number of Nodes} ) + $0.10/hr ) * {Number of Hours}
```

These prices are as of 2021-04-22 and are subject to change. They are shown for estimation purposes only when choosing the right size of cluster for your needs. You can use the [Google Cloud pricing calculator](https://cloud.google.com/products/calculator) for more accurate cost calculations.

### Additional Considerations

Remember that CPU and memory prices per node generally scale linerally so you do not have cost savings by using fewer bigger machines. You should not need to use large machine types unless your container workloads are very CPU and memory intensive, in which case you probably should use this Terraform module as inspiration for creating a custom module that meets your needs.

When using GCP, you can get [sustained use discounts](https://cloud.google.com/compute/docs/sustained-use-discounts) when an instance runs for more than 25% of a month for some instance types. These discounts include usage of `n1` and `n2` instances that are commonly used for cluster node pools.

Google recently introduced the `e2` instances which have cost savings built into the hourly price without duration usage requirements. This is ideal for Kubernetes clusters since nodes should be autoscaling up and down in short time periods based on demand.

## Sizing Calculations

Kubernetes design best practices specify that you can have a maximum of 10 pods per CPU core (0.1 CPU or 100mCPU per pod).

Based on our experience at GitLab with CI/CD pipelines, we have found that most review apps, staging, and production deployments used for demonstration or sandbox purposes use a low amount of resources so we have doubled this number for GitLab use cases to 20 pods per CPU core.

> Keep in mind that most GitLab CI/CD deployments do not have CPU and memory requests as part of the Helm chart so your analytics and monitoring data may not show accurate numbers since it is based on requests and not actual usage. You should use your VM instance monitoring metrics for accurately evaluating actual usage of nodes in a cluster as needed.

Kubernetes will automatically determine the network CIDR range for each node's pods based on the maximum number of pods per node.
* `/27` for 9-16 pods per node
* `/26` for 17-32 pods per node
* `/25` for 33-64 pods per node
* `/24` for 65-110 pods per node (use at your own discretion)

### Small Cluster (/22 CIDR)

A small cluster is designed for demos, sandbox and testing use cases with less than 256 pods.

| `gke_cluster_type`       | `gke_node_count_max` | `gke_node_pods_count_max` | `gcp_machine_type`     |
|--------------------------|----------------------|---------------------------|------------------------|
| `zonal` (recommended)    | `8`                 | `32`                      | `e2-standard-2`         |
| `zonal`                  | `4`                 | `64`                      | `e2-standard-4` or `-8` |
| `zonal`                  | `2`                 | `64` to `110` (k8s limit) | `e2-standard-8` or `-16`|
| `regional`               | `2` (per zone)      | `32`                      | `e2-standard-2`         |

With a `/23` network CIDR (510 IP addresses) for pods across all nodes in the cluster, this allows for the following calculations.

> For easy memorization, we make an opinionated decision to round the number of nodes down for zonal clusters to base 2 numbers since they are easily recognizable. We divide the number by 3 for regional clusters (since nodes are allocated across 3 regions).

`{# of IP addresses in /23 CIDR}` / `{# of IP addresses in node CIDR range}` = `{# of nodes}`

```
510 Pod IPs / 30 Node Range IPs (`/27`) = 17 nodes

Zonal Clusters (round down to nearest base 2) => 16 nodes
Regional Clusters (divided by 3) => 5 nodes
```

| Pods IPs | Node Pod IPs       | Zonal Cluster Node Count | Regional Cluster Node Count |
|----------|--------------------|--------------------------|-----------------------------|
| `510`    | `30` (`/27` CIDR)  | `17` => `16` (rounded)   | `5`                         |
| `510`    | `62` (`/26` CIDR)  | `8`                      | `2`                         |
| `510`    | `126` (`/25` CIDR) | `4`                      | `1`                         |
| `510`    | `254` (`/24`)      | `2`                      | N/A                         |

### Medium Cluster (/20 CIDR)

A medium cluster is designed for scalable production services with less than 1,024 pods.

| `gke_cluster_type`       | `gke_node_count_max` | `gke_node_pods_count_max` | `gcp_machine_type`      |
|--------------------------|----------------------|---------------------------|-------------------------|
| `zonal` (recommended)    | `32`                 | `32`                      | `e2-standard-2`         |
| `zonal`                  | `16`                 | `64`                      | `e2-standard-4` or `-8` |
| `zonal`                  | `8`                  | `64` to `110` (k8s limit) | `e2-standard-8` or `-16`|
| `regional` (recommended) | `10` (per zone)      | `32`                      | `e2-standard-2`         |
| `regional`               | `5` (per zone)       | `64`                      | `e2-standard-4` or `-8` |

With a `/21` network CIDR (2,046 IP addresses) for pods across all nodes in the cluster, this allows for the following calculations.

| Pods IPs | Node Pod IPs       | Zonal Cluster Node Count | Regional Cluster Node Count |
|----------|--------------------|--------------------------|-----------------------------|
| `2,046`  | `30` (`/27` CIDR)  | `68` => `64` (rounded)   | `21`                        |
| `2,046`  | `62` (`/26` CIDR)  | `33` => `32` (rounded)   | `10`                        |
| `2,046`  | `126` (`/25` CIDR) | `16`                     | `5`                         |
| `2,046`  | `254` (`/24`)      | `8`                      | `2`                         |

### Large Cluster (/16 CIDR)

A large cluster is designed for hyperscale production services with multi-tenant architecture up to 16,384 pods. There are relatively few use cases that justify this cluster.

| `gke_cluster_type`       | `gke_node_count_max` | `gke_node_pods_count_max` | `gcp_machine_type`      |
|--------------------------|----------------------|---------------------------|-------------------------|
| `zonal` (recommended)    | `512`                | `32`                      | `e2-standard-2`         |
| `zonal`                  | `256`                | `64`                      | `e2-standard-4` or `-8` |
| `zonal`                  | `128`                | `64` to `110` (k8s limit) | `e2-standard-8` or `-16`|
| `regional` (recommended) | `176` (per zone)     | `32`                      | `e2-standard-2`         |
| `regional`               | `86` (per zone)      | `64`                      | `e2-standard-4` or `-8` |
| `regional`               | `43` (per zone)      | `64` to `110` (k8s limit) | `e2-standard-8` or `-16`|

With a `/17` network CIDR (32,766 IP addresses) for pods across all nodes in the cluster, this allows for the following combinations.

| Pods IPs | Node Pod IPs       | Zonal Cluster Node Count     | Regional Cluster Node Count |
|----------|--------------------|------------------------------|-----------------------------|
| `32,766` | `30` (`/27` CIDR)  | `1,092` => `1,024` (rounded) | `364`                       |
| `32,766` | `62` (`/26` CIDR)  | `528` => `512` (rounded)     | `176`                       |
| `32,766` | `126` (`/25` CIDR) | `260` => `256` (rounded)     | `86`                        |
| `32,766` | `254` (`/24`)      | `129` => `128` (rounded)     | `43`                        |
