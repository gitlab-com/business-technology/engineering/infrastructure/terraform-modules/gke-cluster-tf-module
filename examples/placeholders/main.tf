# [terraform-project]/main.tf

terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = ">= 3.47"
    }
    google-beta = {
      source  = "hashicorp/google-beta"
      version = ">= 3.47"
    }
  }
  required_version = ">= 0.13"
}

# Define the Google Cloud Provider
provider "google" {
  credentials = file("./keys/gcp-service-account.json")
  project     = var.gcp_project
}

# Define the Google Cloud Provider with beta features
provider "google-beta" {
  credentials = file("./keys/gcp-service-account.json")
  project     = var.gcp_project
}

# Get the existing VPC network
data "google_compute_network" "vpc_network" {
  name = var.gcp_vpc_network
}

# Provision a Kubernetes cluster
module "{{cool_purpose_cluster}}" {
  source = "git::https://gitlab.com/gitlab-com/demo-systems/terraform-modules/gcp/gke/gke-cluster-tf-module.git"

  # Required variables
  cluster_name     = "{{cool-purpose-cluster}}"
  description      = "{{Cool purpose cluster}}"
  gcp_network      = data.google_compute_network.vpc_network.self_link
  gcp_project      = var.gcp_project
  gcp_region       = var.gcp_region
  gcp_region_zone  = var.gcp_region_zone
  gke_cluster_cidr = "{{10.x.x.0/16}}"

  # Optional variables with default values
  gcp_machine_type           = "e2-standard-2"
  gcp_preemptible_nodes      = "true"
  gke_autoscaling_profile    = "BALANCED"
  gke_cluster_type           = "zonal"
  gke_node_count_max         = 32
  gke_node_count_min         = 1
  gke_node_pods_count_max    = 32
  gke_maintenance_start_time = "03:00"
  gke_release_channel        = "REGULAR"
  gke_version_prefix         = "1.18."

  # Labels for metadata and cost analytics
  labels = {
    "{{label_key1}}" = "{{label-value1}}"
    "{{label_key2}}" = "{{label-value2}}"
  }
}
