# GKE Cluster Terraform Module Changelog

## 0.2.0

* [#2](https://gitlab.com/gitlab-com/demo-systems/terraform-modules/gcp/gke/gke-cluster-tf-module/-/issues/2) Update subnets to use dynamic subnet CIDR size and numbering
* [#3](https://gitlab.com/gitlab-com/demo-systems/terraform-modules/gcp/gke/gke-cluster-tf-module/-/issues/3) Remove GitLab infrastructure standards labels from GCP labels map
* [#4](https://gitlab.com/gitlab-com/demo-systems/terraform-modules/gcp/gke/gke-cluster-tf-module/-/issues/4) Remove GitLab infrastructure standards labels from GCP labels map
* [#5](https://gitlab.com/gitlab-com/demo-systems/terraform-modules/gcp/gke/gke-cluster-tf-module/-/issues/5) Update Terraform minimum version to v0.13
* [#6](https://gitlab.com/gitlab-com/demo-systems/terraform-modules/gcp/gke/gke-cluster-tf-module/-/issues/6) Refactor README example usage into examples/ directory structure to align with Terraform module best practices

## 0.1.0

* Initial release with tested resources
